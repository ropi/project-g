﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningScript : MonoBehaviour {

    private Interactive affected;

    // Use this for initialization
    void Start () {
        affected = null;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnParticleCollision(GameObject afetado)
    {
        print("lighting colliding");
        affected = afetado.GetComponent<Interactive>();
        affected.setFire();

    }

}
