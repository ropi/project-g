﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogScript : MonoBehaviour {

    private Interactive affected;

    // Use this for initialization
    void Start () {
        affected = null;
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void OnParticleCollision(GameObject afetado)
    {
        print("fog colliding");
        affected = afetado.GetComponent<CreepScript>();
        affected.setBlinded();
        
    }

}
