﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoolWindScript : MonoBehaviour {

    private Interactive affected;

	// Use this for initialization
	void Start () {
        affected = null;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void OnParticleCollision(GameObject afetado)
    {
        affected = afetado.GetComponent<Interactive>();
        affected.LowerMovespeed(0.5f);
        affected.BeingAffected();
        print("wind colliding");

    }

    void OnTriggerExit(Collider victimCollider)
    {
        victimCollider.gameObject.GetComponent<Interactive>().Unaffected();
    }

}
