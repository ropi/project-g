﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YounGenTech.HealthScript;

public class CreepScript : Interactive {

    public float fpsTargetDistance;
    public float enemyLookDistance;
    public float attackDistance;
    public float enemyFollowDistance;
    //moveSpeed vem de Interactive
    public float damping;
    private GameObject fpsPlayer;
    private Transform fpsTarget;
    private float attackDelay;
    private float attackTime;
    Rigidbody rb;
    Renderer rend;
    public ParticleSystem fire;
    private bool die;


    // Use this for initialization
    void Start () {
        sendoAfetado = false;
        onFire = false;
        fireTimer = 0f;
        rend = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody>();
        defaultSpeed = 0.15f;
        moveSpeed = defaultSpeed;
        damping = 5f;
        attackDistance = 2f;
        enemyLookDistance = 11;
        enemyFollowDistance = 9;
        attackDelay = 2f;
        attackTime = 0f;
        fire = Instantiate(fire, this.gameObject.transform);
        fire.Stop(true);
        die = false;
        blinded = false;
        StartCoroutine("BlindCount");
        fpsPlayer = GameObject.Find("Player one");
        fpsTarget = fpsPlayer.transform;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        
            fpsTargetDistance = Vector3.Distance(fpsTarget.position, transform.position);
            if (fpsTargetDistance < enemyLookDistance && blinded == false)
            {
                lookAtPlayer();
            }
            if (fpsTargetDistance < enemyFollowDistance && fpsTargetDistance > attackDistance && blinded == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, fpsTarget.position, moveSpeed);
            }
            if (fpsTargetDistance < attackDistance)
            {
                if (attackTime - Time.time <= 0)
                {
                    atacar();
                    attackTime = Time.time + attackDelay;
                }

            }
        
        //if(blindedTime - Time.time <= 0)
        //{
        //    blinded = false;
        //}

        if (sendoAfetado == false)
        {
            moveSpeed = defaultSpeed;
        }
        if (fireTimer == 3f)
        {
            fire.Play(true);
            die = true;
        }

        if (fireTimer > 0f)
        {
            fireTimer -= Time.deltaTime;
        }
        else
        {
            if(die == true)
            {
                Destroy(gameObject);
            }
            onFire = false;
        }
        
    }

    void lookAtPlayer()
    {
        Quaternion rotation = Quaternion.LookRotation(fpsTarget.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }

    void atacar()
    {
        Health health = fpsPlayer.GetComponent<Health>();

        if (health)
            health.Damage(new HealthEvent(gameObject, 10f));
    }

    

}
