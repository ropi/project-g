﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraVRScript : MonoBehaviour {

    public ParticleSystem geteff;
    public Text text;
    private int counter;

	// Use this for initialization
	void Start () {
        geteff = Instantiate(geteff, Vector3.zero, Quaternion.identity);
        counter = 0;
    }
	
	// Update is called once per frame
	void Update () {
        RaycastHit seen;
        Ray raydirection = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(raydirection, out seen, 2.5f))
        {
            if (seen.collider.tag == "totem")
            {
                geteff.transform.SetPositionAndRotation(seen.collider.transform.position, Quaternion.Euler(new Vector3(-90f, 0f, 0f)));
                geteff.Emit(1);
                Destroy(seen.collider.gameObject);
                counter++;
                text.text = counter.ToString();
            }

        }
        //Debug.DrawRay(transform.position, transform.forward, Color.black, 1); //unless you allow debug to be seen in game, this will only be viewable in the scene view
    }

}
