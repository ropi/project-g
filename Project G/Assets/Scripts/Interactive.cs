﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactive : MonoBehaviour {

    public float moveSpeed;
    public bool sendoAfetado;
    public float defaultSpeed;
    public bool onFire;
    public float fireTimer;
    public bool blinded;

    // Use this for initialization
    void Start () {
        sendoAfetado = false;
        onFire = false;
        fireTimer = 0f;
        blinded = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (fireTimer > 0f)
        {
            fireTimer -= Time.deltaTime;
        }
        else
        {
            onFire = false;
        }
	}

    public void LowerMovespeed(float decrease)
    {
        if(moveSpeed>defaultSpeed*0.5f)
            moveSpeed = moveSpeed*decrease;
    }

    public void BeingAffected()
    {
        sendoAfetado = true;
    }

    public void Unaffected()
    {
        sendoAfetado = false;
    }

    public void setFire()
    {
        fireTimer = 3;
        onFire = true;
    }

    public void setBlinded()
    {
            blinded = true;
    }

    IEnumerator BlindCount()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            blinded = false;
        }
    }

}
