﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YounGenTech.HealthScript;

public class CanvasUIScript : MonoBehaviour {

    private Animator animator;
    private GameObject player;
    private Health health;
    private GameObject counter;
    private Text contador;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        player = GameObject.Find("Player one");
        health = player.GetComponent<Health>();
        counter = GameObject.Find("totemCount");
        contador = counter.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        //morte
        if (health.Value == 0f||Time.time > 600f)
        {
            animator.SetBool("Dead", true);
        }
        if (Int32.Parse(contador.text) > 14)
        {
            animator.SetBool("Win", true);
        }
    }
}
