﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTwoGUI : MonoBehaviour {

    public ParticleSystem skillArea;
    private ParticleSystem skillAreaClone;
    public ParticleSystem skill2Area;
    private ParticleSystem skill2AreaClone;
    public ParticleSystem skill3Area;
    private ParticleSystem skill3AreaClone;
    public Text contador;

    public GameObject goTerrain;
    GameObject afetado;

    private Interactive affected;

    public Camera gcamera;
    public Texture2D skill1Icon;
    public Texture2D skill2Icon;
    public Texture2D skill2cdIcon;
    public Texture2D skill3Icon;

    float skill2timer;
    float skill2cd;
    

    // Use this for initialization
    void Start () {
        skillAreaClone = Instantiate(skillArea, Vector3.zero, Quaternion.identity);
        skill2AreaClone = Instantiate(skill2Area, Vector3.zero, Quaternion.identity);
        skill3AreaClone = Instantiate(skill3Area, Vector3.zero, Quaternion.identity);
        skill2AreaClone.Stop(true);
        skill3AreaClone.Stop(true);
        skill2timer = 0;
        skill2cd = 2;
    }
	
	// Update is called once per frame
	void Update () {
        if (Int32.Parse(contador.text) > 2)
        {
            bool skill1hold = Input.GetKey(KeyCode.Q);
            if (skill1hold)
            {
                Skill1();
            }
        }
        if (Int32.Parse(contador.text) > 5)
        {
            bool skill2key = Input.GetKeyDown(KeyCode.W);

            if (skill2key)
            {
                if (skill2timer <= 0)
                {
                    Skill2();
                    skill2timer = skill2cd;
                }
            }
            if(skill2timer>0) skill2timer -= Time.deltaTime;
        }
        if (Int32.Parse(contador.text) > 8)
        {
            bool skill3hold = Input.GetKey(KeyCode.E);
            if (skill3hold)
            {
                Skill3();
            }
            else skill3AreaClone.Stop(true);
        }

        
        
    }

    void OnGUI()
    {
        if (Int32.Parse(contador.text) > 2)
            GUI.Label(new Rect(10, 10, 50, 50), skill1Icon);
        if (Int32.Parse(contador.text) > 8)
            GUI.Label(new Rect(130, 10, 50, 50), skill3Icon);
        if (Int32.Parse(contador.text) > 5)
            if (skill2timer <= 0)
            {
            GUI.Label(new Rect(70, 10, 50, 50), skill2Icon);
            }
            else
            {
            GUI.Label(new Rect(70, 10, 50, 50), skill2cdIcon);
            }
    }

    void Skill1()
    {
        RaycastHit hit;
        Ray ray = gcamera.ScreenPointToRay(Input.mousePosition);
        if (goTerrain.GetComponent<Collider>().Raycast(ray, out hit, Mathf.Infinity)) {
            skillAreaClone.transform.SetPositionAndRotation(hit.point, Quaternion.Euler(new Vector3(0f,-90f,0f)));
            skillAreaClone.Emit(1);

        }
    }

    void Skill2()
    {
        RaycastHit hit;
        Ray ray = gcamera.ScreenPointToRay(Input.mousePosition);
        if (goTerrain.GetComponent<Collider>().Raycast(ray, out hit, Mathf.Infinity))
        {
            skill2AreaClone.transform.SetPositionAndRotation(hit.point, Quaternion.Euler(new Vector3(0f, -90f, 0f)));
            
            if(skill2AreaClone.isPlaying == false)
            {
                skill2AreaClone.Play(true);
            }
            
        }
    }

    void Skill3()
    {
        RaycastHit hit;
        Ray ray = gcamera.ScreenPointToRay(Input.mousePosition);
        if (goTerrain.GetComponent<Collider>().Raycast(ray, out hit, Mathf.Infinity))
        {
            skill3AreaClone.transform.SetPositionAndRotation(hit.point+new Vector3(0f,1.2f,0f), Quaternion.Euler(new Vector3(-90f, 0f, 0f)));
            if(skill3AreaClone.isPlaying == false)
                skill3AreaClone.Play(true);

        }
    }

    
}
