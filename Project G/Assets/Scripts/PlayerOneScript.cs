﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YounGenTech.HealthScript;

public class PlayerOneScript : Interactive
{
    private float jumpSpeed;

    private float moveWS, moveAD, verticalSpeed;

    private CharacterController player;
    private Animator animator;
    
    private bool jumpedOnce;
    private bool jumpedTwice;
    private bool fireFlag;
    private Health health;

    public Transform visao;

    public ParticleSystem fire;

    // Use this for initialization
    void Start()
    {
        jumpedOnce = false;
        jumpedTwice = false;
        sendoAfetado = false;
        onFire = false;
        fireTimer = 0f;
        jumpSpeed = 7.0f;
        defaultSpeed = 10.0f;
        moveSpeed = defaultSpeed;
        player = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        fire = Instantiate(fire, this.gameObject.transform);
        fire.Stop(true);
        health = GetComponent<Health>();
    }
    

    // Update is called once per frame
    void Update()
    {
        if(fireTimer == 3f)
        {
            fire.Play(true);
            health.Damage(new HealthEvent(gameObject, 5f));
        }
        if (fireTimer > 0f)
        {
            fireTimer -= Time.deltaTime;
        }
        else
        {
            onFire = false;
        }
        if (sendoAfetado == false)
        {
            moveSpeed = defaultSpeed;
        }

        if (onFire == true)
        {
            moveSpeed = moveSpeed * 2;
        }
        else fire.Stop(true);

        if (health.Value != 0)
        {
            Movimento();
        }
        Gravity();

        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }

        transform.rotation = new Quaternion(transform.rotation.x, visao.rotation.y, transform.rotation.z, transform.rotation.w);
        
    }

    void Movimento()
    {
        moveWS = Input.GetAxis("Vertical") * moveSpeed *0.7f;
        moveAD = Input.GetAxis("Horizontal") * moveSpeed * 0.7f;

        if (Input.GetAxis("Horizontal") > 0)
        {
            animator.SetBool("Side R", true);
            animator.SetBool("Side L", false);
        } else if (Input.GetAxis("Horizontal") < 0)
        {
            animator.SetBool("Side L", true);
            animator.SetBool("Side R", false);
        }
        else {
            animator.SetBool("Side R", false);
            animator.SetBool("Side L", false);
        }

        if(Input.GetAxis("Vertical") > 0)
        {
            animator.SetBool("Running", true);
            animator.SetBool("Running B", false);
        }
        else if(Input.GetAxis("Vertical") < 0)
        {
            animator.SetBool("Running B", true);
            animator.SetBool("Running", false);
        } else
        {
            animator.SetBool("Running", false);
            animator.SetBool("Running B", false);
        }

        //correr
        if (Input.GetAxis("Sprint")>0)
        {
            moveWS = moveWS * 2;
            moveAD = moveAD * 1.5f;
            animator.SetBool("Sprint", true);
        }
        else
        {
            animator.SetBool("Sprint", false);
        }
        //slow walk
        if (Input.GetKey(KeyCode.LeftControl))
        {
            moveWS = moveWS * 0.6f;
            moveAD = moveAD * 0.6f;
        }
        
        Vector3 movement = new Vector3(0, verticalSpeed, 0);
        transform.position += transform.forward * Time.deltaTime * moveWS;
        transform.position += transform.right * Time.deltaTime * moveAD;
        player.Move(movement * Time.deltaTime);
        

    }

    void Jump()
    {
        if (jumpedTwice == false)
        {
            verticalSpeed = jumpSpeed;
            if (jumpedOnce == true)
            {
                jumpedTwice = true;
                animator.SetTrigger("Jump");
            }
            else
            {
                jumpedOnce = true;
                animator.SetTrigger("Jump");
            }
        }

    }

    void Gravity()
    {
        if(player.isGrounded == false)
        {
            animator.SetBool("Grounded", false);
            verticalSpeed += Physics.gravity.y * Time.deltaTime * 1.5f;
            animator.SetTrigger("Jump");
        } else
        {
            animator.SetBool("Grounded", true);
            jumpedOnce = false; jumpedTwice = false;
        }
    }

}
